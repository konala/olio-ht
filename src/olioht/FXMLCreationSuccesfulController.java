/*
  Controller for package creationg succesful window
 */
package olioht;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Konsta
 */
public class FXMLCreationSuccesfulController implements Initializable {
    @FXML
    private Button creationOKButton;
    @FXML
    private ListView<String> listView;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        listView.getItems().add("Luotiin paketti seuraavilla tiedoilla:");
        listView.getItems().add("Nimi: " + FXMLCreatePackageController.item.getName());
        listView.getItems().add("Tilavuus: " + FXMLCreatePackageController.item.getVolume() + " cm3");
        listView.getItems().add("Paino: " + FXMLCreatePackageController.item.getWeight() + " kg");
        listView.getItems().add("Luokka: " + FXMLCreatePackageController.packageClass);
        listView.getItems().add("Lähtöpaikka:  " + FXMLCreatePackageController.departurePost);
        listView.getItems().add("Määränpää:  " + FXMLCreatePackageController.arrivalPost);
        listView.getItems().add("Välimatka: " + FXMLCreatePackageController.routeLength + " km");
    }
    @FXML
    private void closeWindow(ActionEvent event) {
        Stage stage =(Stage) creationOKButton.getScene().getWindow();
        stage.close();
        
    }
    
}
