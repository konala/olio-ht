/*Controller for the program's starting window*/
package olioht;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 *
 * @author Konsta
 */
public class FXMLDocumentController implements Initializable {
    
    //JavaFX components
    @FXML
    private WebView mapView;
    @FXML
    private ComboBox<String> smartPostCombobox;
    @FXML
    private Button addPostToMapButton;
    @FXML
    private Button createPackageButton;
    @FXML
    private Button deletePathsButton;
    @FXML
    private ComboBox<String> choosePackageCombobox;
    @FXML
    private ListView<String> packageInfoListView;
    @FXML
    private Button sendPackageButton;
    @FXML
    private TitledPane sendPackagesTab;
    @FXML
    private Button searchButton;
    @FXML
    private TextField searchTextField;
    @FXML
    private Button addDestAndArrToChartButton;
    @FXML
    private ComboBox<String> kioskCombobox;
    @FXML
    private TitledPane logTabPane;
    @FXML
    private TextArea logTextArea;
    @FXML
    private ImageView imageView;
    
    //Other variables
    private Package p; //p points to the package currently handled
    public static int errorCode = 0; //0 = package remained intact, 1 = package was broken during transportation, 2 = package was already sent, 3 = no package was found, 4 = no choice made
    private Log log = Log.getInstance(); //instance of Log which keeps log up to date and finally prints "log.txt"
    ArrayList<SmartPost> posts = new ArrayList<>(); //used to initialize & update comboboxes
   
   /*Loads map, reads XML with DataBuilder, loads the TIMO logo, initializes combobox with different cities */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        mapView.getEngine().load(getClass().getResource("index.html").toExternalForm());
        DataBuilder db = new DataBuilder("http://smartpost.ee/fi_apt.xml");
        Image image = new Image(getClass().getResource("timo_logo.png").toExternalForm());
        imageView.setImage(image);
        
        posts = SmartPost.getSmartPosts();
        for (SmartPost s : posts) {
            if (!(smartPostCombobox.getItems().contains(s.getCity())))
                smartPostCombobox.getItems().add(s.getCity());
        }
        
        smartPostCombobox.getItems().sort(null);
        packageInfoListView.getItems().add("Tiedot valitusta paketista:");

    }    

    /*When the button "lisää kartalle" is pressed this function draws a marker on the map */
    @FXML
    private void addSmartPostToMap(ActionEvent event) {
        
        SmartPost s = null;
        
        //Checks if the two comboboxes actually have a value selected
        if (kioskCombobox.getValue() != null) {
            s = SmartPost.getSmartPost(kioskCombobox.getValue());
        }
        //Runs javascript
        if (s != null) {
            String script = "document.goToLocation('"+ s.getAddress() +  "," + s.getCode() + " " + s.getCity() + "','" + s.getPostoffice() + " " + s.getAvailability() + "','blue')";
            mapView.getEngine().executeScript(script);
        }
        else {
            //Pops up error if something went wrong
            errorCode = 4;
            try {
                Stage warning = new Stage();
                Parent page = FXMLLoader.load(getClass().getResource("FXMLPackageSent.fxml"));
                Scene scene = new Scene(page);
                scene.getStylesheets().add(getClass().getResource("CSSCreatePackage.css").toExternalForm());
                Image check; 
                warning.setTitle("Virhe");
                check = new Image(getClass().getResource("warning_icon.png").toExternalForm());
                warning.getIcons().add(check);
                warning.setScene(scene);
           
                warning.show();
            
            } catch (IOException ex) {
                Logger.getLogger(FXMLCreatePackageController.class.getName()).log(Level.SEVERE, null, ex);
         }
        }
        errorCode = 0;
    }

    
    /* Same as above but this function searches for smartpost according to user input*/
    @FXML
    private void addSmartPostToMap2(ActionEvent event) {
        
        SmartPost s;
        if (searchTextField.getText().equals(""))
            s = null;
        else
            s = SmartPost.getSmartPost(searchTextField.getText());
        
        if (s != null) {
            System.out.println(searchTextField.getText());
            String script = "document.goToLocation('"+ s.getAddress() +  "," + s.getCode() + " " + s.getCity() + "','" + s.getPostoffice() + " " + s.getAvailability() + "','blue')";
            mapView.getEngine().executeScript(script);
        } else {
            errorCode = 4;
            try {
                Stage warning = new Stage();
                Parent page = FXMLLoader.load(getClass().getResource("FXMLPackageSent.fxml"));
                Scene scene = new Scene(page);
                scene.getStylesheets().add(getClass().getResource("CSSCreatePackage.css").toExternalForm());
                Image check; 
                warning.setTitle("Virhe");
                check = new Image(getClass().getResource("warning_icon.png").toExternalForm());
                warning.getIcons().add(check);
                warning.setScene(scene);
           
                warning.show();
            
            } catch (IOException ex) {
                Logger.getLogger(FXMLCreatePackageController.class.getName()).log(Level.SEVERE, null, ex);
         }
        }
        errorCode = 0;
    }
    
    
    /* Pops up another window for creating new package */
    @FXML
    private void createPackage(ActionEvent event) {
        try {
            Stage createPackageView = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("FXMLCreatePackage.fxml"));
            Scene scene = new Scene(page);
            scene.getStylesheets().add(getClass().getResource("CSSCreatePackage.css").toExternalForm());
            Image icon = new Image(getClass().getResource("createPackage_icon.png").toExternalForm());
            createPackageView.getIcons().add(icon);
            createPackageView.setTitle("Paketin luominen");
            createPackageView.setScene(scene);
            createPackageView.show();
        } catch (IOException e) {
            Logger.getLogger(FXMLCreatePackageController.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /* Deletes all drawn paths using javascript */
    @FXML
    private void deletePaths(ActionEvent event) {
        mapView.getEngine().executeScript("document.deletePaths()");
    }
  
    /* Draws a path on the map when Lähetä paketti -button is pressed */
    @FXML
    private void drawPath(ActionEvent event) throws InterruptedException {
        
        p = Storage.getPackage(choosePackageCombobox.getValue()); //Currently selected package
        
        if (p != null) { //Current package must not be null
            
            if (p.state.equals("Odottaa lähetystä")) { //Checks whether current package has already been sent
                
                double departureLongitude = Double.parseDouble(SmartPost.getSmartPost(Storage.getDeparture(p.packageID)).getLongitude()); //Coordinates 
                double departureLatitude = Double.parseDouble(SmartPost.getSmartPost(Storage.getDeparture(p.packageID)).getLatitude());
                double arrivalLongitude = Double.parseDouble(SmartPost.getSmartPost(Storage.getArrival(p.packageID)).getLongitude());
                double arrivalLatitude = Double.parseDouble(SmartPost.getSmartPost(Storage.getArrival(p.packageID)).getLatitude());
                String color = "blue";
                int clss = Storage.getClss(p.packageID);
               
                //Switches the color to draw with according to class and checks whether the package will break or not
                switch(clss) {
                    case 1:
                        color = "red";
                        if (p.contents.isFragile())
                            errorCode = 1;
                        break;
                    case 2:
                        color = "yellow";
                        break;
                    case 3:
                        color = "green";
                        if (p.contents.isFragile())
                            errorCode = 1;
                        break;
                    default:
                        color = "blue";
                }
                
                //Drawing
                String script = "document.createPath([" + departureLatitude + "," + departureLongitude + "," + arrivalLatitude + "," + arrivalLongitude + "],'" + color + "'," + clss + ")";
                mapView.getEngine().executeScript(script);
                p = Storage.getPackage(choosePackageCombobox.getValue());
                p.changeState(1); //Change package state to sent (=lähetetty)
                
                //Refresh package info
                ActionEvent e = (ActionEvent)sendPackagesTab.getOnMousePressed();
                refreshPackageInfo(e);
                Storage.removePackage(p); //Removes sent package from storage
                
                //Writing to log, 2=package broke, 1=package remained intact
                if (p.contents.isFragile())
                    log.writeToLog(p, 2);
                else
                    log.writeToLog(p, 1);
                
                //Open the correct packageSentInterface(), depends on errorCode
                packageSentInterface();
                
            } else {
                errorCode = 2;
                packageSentInterface();
            }
        } else {
            errorCode = 3;
            packageSentInterface();
        }
        
    }

     /* Resfreshes packages in choosePackageCombobox */
    @FXML
    private void refreshPackages(MouseEvent event) {
        choosePackageCombobox.getItems().clear();
        for (Package fc : Storage.firstClassPackages) {
            choosePackageCombobox.getItems().add(fc.contents.getName());
        }
        for (Package sc : Storage.secondClassPackages) {
            choosePackageCombobox.getItems().add(sc.contents.getName());
        }
        for (Package tc : Storage.thirdClassPackages) {
            choosePackageCombobox.getItems().add(tc.contents.getName());
        }
    }
    
    /* Refreshes the package info shown in "Pakettien lähetys" tab */
    @FXML
    private void refreshPackageInfo(ActionEvent event) {
        packageInfoListView.getItems().clear();
        p = Storage.getPackage(choosePackageCombobox.getValue());
        if (p != null) {
            packageInfoListView.getItems().add("Tiedot valitusta paketista:");
            packageInfoListView.getItems().add("Nimi: " + p.contents.getName());
            packageInfoListView.getItems().add("Tilavuus: " + p.contents.getVolume() + " cm3");
            packageInfoListView.getItems().add("Paino: " + p.contents.getWeight()+ " kg");
            packageInfoListView.getItems().add("Luokka: " + Storage.getClss(p.packageID));
            packageInfoListView.getItems().add("Lähtöpaikka: " + p.departurePost.getAddress() + " " + p.departurePost.getCity());
            packageInfoListView.getItems().add("Määränpää: " + p.arrivalPost.getAddress() + " " + p.arrivalPost.getCity());
            packageInfoListView.getItems().add("Välimatka: " + p.distance + " km");
            packageInfoListView.getItems().add("Tila: " + p.state);
        }
    }
    
    /* Opens another window when a package is sent */
    private void packageSentInterface() {
        try {
            Stage packageSent = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("FXMLPackageSent.fxml"));
            Scene scene = new Scene(page);
            scene.getStylesheets().add(getClass().getResource("CSSCreatePackage.css").toExternalForm());
            Image check;
            
            //Choose title and icon according to errorCode
            switch(errorCode) {
                case 0:
                    packageSent.setTitle("Paketti lähetetty");
                    check = new Image(getClass().getResource("check.png").toExternalForm());
                    break;
                case 1:
                    packageSent.setTitle("Paketti hajosi!");
                    check = new Image(getClass().getResource("warning_icon.png").toExternalForm());
                    break;
                case 2:
                    packageSent.setTitle("Paketti on jo lähetetty");
                    check= new Image(getClass().getResource("warning_icon.png").toExternalForm());
                    break;
                case 3:
                    packageSent.setTitle("Pakettia ei löytynyt");
                    check= new Image(getClass().getResource("warning_icon.png").toExternalForm());
                    break;
                default:
                    packageSent.setTitle("Virhe");
                    check= new Image(getClass().getResource("warning_icon.png").toExternalForm());
                    
            }
            packageSent.getIcons().add(check);
            packageSent.setScene(scene);
            packageSent.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLCreatePackageController.class.getName()).log(Level.SEVERE, null, ex);
        }
        errorCode = 0;
    }

        
    /* search function for searching smartposts */
    @FXML
    private void searchSmartPost(ActionEvent event) {
        System.out.println("Etsitään..");
        String address = searchTextField.getText();
        SmartPost sp = null;
        if (!(address.equals("")))
            sp = SmartPost.getSmartPost(address);
        if (sp != null) {
            searchTextField.setText(sp.getAddress() + " " + sp.getCity());
        } else {
            searchTextField.setText("Postia ei löytynyt");
            System.out.println("Ei löytyny.");
        }
        
    }

    
    /* the functionality of "Lähtö ja määränpää kartalle"-button: adds both departure and arrival posts on the map */
    @FXML
    private void addDestAndArrToChart(ActionEvent event) {
        SmartPost d = null;
        SmartPost a = null;
        if (choosePackageCombobox.getValue() != null) {
            d = Storage.getPackage(choosePackageCombobox.getValue()).departurePost;
            a = Storage.getPackage(choosePackageCombobox.getValue()).arrivalPost;
        } else {
            errorCode = 3;
            packageSentInterface();
        }
        if (d != null && a != null) {
            String script = "document.goToLocation('"+ d.getAddress() +  "," + d.getCode() + " " + d.getCity() + "','" + d.getPostoffice() + " " + d.getAvailability() + "','blue')";
            String script2 = "document.goToLocation('"+ a.getAddress() +  "," + a.getCode() + " " + a.getCity() + "','" + a.getPostoffice() + " " + a.getAvailability() + "','blue')";
            mapView.getEngine().executeScript(script);
            mapView.getEngine().executeScript(script2);
        }
        else
            System.out.println("Virhe");
        
    }

    
    /* Refreshes the contents of "Valitse automaatti" -combobox in "Lisää automaatti" -tab according to the city chosen */
    @FXML
    private void refreshKioskCombobox(ActionEvent event) {
        kioskCombobox.getItems().clear();
        if (smartPostCombobox.getValue() != null) {
            for (SmartPost sp : SmartPost.getSmartPosts()) {
                if (sp.getCity().equals(smartPostCombobox.getValue()))
                    kioskCombobox.getItems().add(sp.getPostoffice() + ", " + sp.getAddress());
            }
            
        }
    }

    /* Updates the textual contents of the log found in "Loki"-tab */
    @FXML
    private void updateLog(MouseEvent event) {
        for (String s : log.getEvents()) {
            if (!(logTextArea.getText().contains(s)))
                logTextArea.appendText(s);
        }
            
    }

    
}
