/*
 Singleton class for writing log in "Loki" tab and to "log.txt" file
 */

package olioht;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * Konsta Ala-Ilomäki, 0418514
 */
public class Log {
    private static Log log = null;
    private ArrayList<String> events = new ArrayList<>(); 
    private Date time;
    
    private Log() {  
    }
    
    public static Log getInstance() {
        if (log == null) {
            log = new Log();
        }
        return log;
    }

    public ArrayList<String> getEvents() {
        return events;
    }
    

    /**
     * 
     * @param p the package that we are logging about
     * @param i refers to the type of event, 0 = creation of a package, 1 = package sent and received (remained intact), 2 = package sent and received (broke on the way)
     */
        public void writeToLog(Package p, int i) {
        time = new Date();
        switch(i) {
            case 0:
                events.add("\n##############################################\n" + p.getTimeOfCreation() +"\nLuotiin uusi paketti:\n\tPaketin ID: " + p.getPackageID()+ "\n\tSisältö: " + p.contents.getName() + "\n\tLuokka: " + p.getPackageClass() +"\n\tTilavuus ja paino: " +
                        p.contents.getVolume() + " cm3 "+ p.contents.getWeight() + 
                        " kg\n\tLähtö: " + p.departurePost.getAddress()+ ", "+ p.departurePost.getCity() + "\n\tMääränpää: " + p.arrivalPost.getAddress() + ", "+ p.arrivalPost.getCity() 
                        + "\n\tVälimatka: " + p.distance + " km" + 
                        "\n\tSärkyvä: " + p.contents.isFragile() + "\n\tTila: " + p.state + "\n\tPaketteja varastossa: " + Storage.whatsInStorage()  + " kpl");
                break;
            case 1:
                events.add("\n##############################################\n" + time +"\nPaketti lähetettiin ja se säilyi ehjänä:\n\tPaketin ID: " + p.getPackageID()+ "\n\tSisältö: " + p.contents.getName()+ "\n\tLuokka: " + p.getPackageClass() 
                        + "\n\tTilavuus ja paino: " + p.contents.getVolume() + " cm3 "+ p.contents.getWeight() + 
                        " kg\n\tLähtö: " + p.departurePost.getAddress()+ ", "+ p.departurePost.getCity() + "\n\tMääränpää: " + p.arrivalPost.getAddress() + ", "+ p.arrivalPost.getCity() 
                        + "\n\tVälimatka: " + p.distance + " km" + 
                        "\n\tSärkyvä: " + p.contents.isFragile() + "\n\tTila: " + p.state + "\n\tPaketteja varastossa: " + Storage.whatsInStorage()  + " kpl");
                break;
            case 2:
                events.add("\n##############################################\n" + time +"\nPaketti lähetettiin ja se rikkoutui matkalla:\n\tPaketin ID: " + p.getPackageID()+ "\n\tSisältö: " + p.contents.getName() +  "\n\tLuokka: " + p.getPackageClass()
                        + "\n\tTilavuus ja paino: " + p.contents.getVolume() + " cm3 "+ p.contents.getWeight() + 
                        " kg\n\tLähtö:  " + p.departurePost.getAddress()+ ", "+ p.departurePost.getCity() + "\n\tMääränpää: " + p.arrivalPost.getAddress() + ", "+ p.arrivalPost.getCity() 
                        + "\n\tVälimatka: " + p.distance + " km" + 
                        "\n\tSärkyvä: " + p.contents.isFragile() + "\n\tTila: " + p.state + "\n\tPaketteja varastossa: " + Storage.whatsInStorage()  + " kpl");
                break;
            default:
                events.add("Virhe");
        }
        
    }
    
    /* Creates log file "log.txt" in user's home directory (System.getProperty("user.home")) */
    public void writeLogToFile() {
        OutputStreamWriter osw;
        String homeDir = System.getProperty("user.home");
        String path = homeDir + File.separator + "log.txt";
            
        
        try {
            OutputStream os = new FileOutputStream(path, true);
            osw = new OutputStreamWriter(os);
            for (String s : events) {
                osw.append(s);
            }
            osw.close();
        } catch (IOException ex) {
            Logger.getLogger(Log.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    
}
