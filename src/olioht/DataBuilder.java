/*
Class for handling XML data
 */

package olioht;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * Konsta Ala-Ilomäki, 0418514
 */
public class DataBuilder {
    
    private Document doc;
    protected URL url;
    private String distance;

    /**
     *
     * @param s URL to read XML from, used to read smartpost data 
     */
    public DataBuilder(String s) {
        try {
            System.out.println("Haetaan XML osoitteesta " + s);
            url = new URL(s);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(new InputSource(new StringReader(readXML())));
            doc.getDocumentElement().normalize();
            parseData();
        } catch (SAXException | IOException | ParserConfigurationException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Reads distance from XML that was provided by google api
     * @param path the coordinates between two smartposts: [start longitude, start latitude, end longitude, end latitude]
     */
    public DataBuilder(ArrayList<String> path) {
        try {
            url = new URL("https://maps.googleapis.com/maps/api/distancematrix/xml?origins="+path.get(0)+","+path.get(1)+"&destinations="+path.get(2)+","+path.get(3));
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(new InputSource(new StringReader(readXML())));
            doc.getDocumentElement().normalize();
            parseDistance();
        } catch (SAXException | IOException | ParserConfigurationException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private String readXML() {
        String line, content = "";
        BufferedReader br;
        try {
            br = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"));
            while ((line = br.readLine()) != null) {
                content += line + "\n";
            }  
            br.close();
        } catch (IOException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
        
                  
     
        
        System.out.println("XML luettiin onnistuneesti.");
        return content;
    }
    
    private void parseData() {
        NodeList nodes = doc.getElementsByTagName("place");
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            SmartPost sp = new SmartPost(getValue(e, "code"), getValue(e, "city"), getValue(e, "address"), getValue(e, "postoffice"), getValue(e, "availability"), getValue(e, "lat"), getValue(e, "lng"));
            
        }
        
        System.out.println("Data parsittiin onnistuneesti.");
    }
    
    private void parseDistance() {
        NodeList nodes = doc.getElementsByTagName("distance");
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            distance = getValue(e, "value");
        }
    }
    
    private String getValue(Element e, String tag) {
        return (e.getElementsByTagName(tag).item(0).getTextContent());
    }
    
    
    public String getDistance() {
        return distance;
    }
}
