/*
Mainclass
 */
package olioht;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *
 * @author Konsta
 */
public class Mainclass extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        System.setProperty("file.encoding", "UTF-8");
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        
        Scene scene = new Scene(root);
        scene.getStylesheets().add(getClass().getResource("CSSMainWindow.css").toExternalForm());
        stage.setScene(scene);
        Image icon = new Image(getClass().getResource("icon.png").toExternalForm());
        stage.getIcons().add(icon);
        stage.setTitle("TIMO");
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.setProperty("file.encoding", "UTF-8");
        Log log = Log.getInstance();
        launch(args);
        log.writeLogToFile();
        System.out.println("Suljetaan...");
    }
    
}
