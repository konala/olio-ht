/*
Class for items
 */

package olioht;

import java.util.ArrayList;

/**
 *
 * Konsta Ala-Ilomäki, 0418514
 */

public class Item {
    private static ArrayList<Item> items = new ArrayList<>(); //keeps track of all created items
    private double weight;
    private double volume;
    private String name;
    private boolean fragile;
    
    
    public Item(double w, double v, String n, boolean f) {
        this.weight = w;
        this.volume = v;
        this.name = n;
        this.fragile = f;
        items.add(this);
    }
    
    public static ArrayList<Item> getItems() {
        return items;
    }
    public static Item getItem(String s) {
        for (Item i : items) {
            if (i.name.equals(s))
                return i;
        }
        return null;
    }
    public double getWeight() {
        return weight;
    }

    public double getVolume() {
        return volume;
    }

    public String getName() {
        return name;
    }

    public boolean isFragile() {
        return fragile;
    }
   
    
    
}
