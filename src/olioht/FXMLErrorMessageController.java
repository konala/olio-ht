/*
Controller for error messages 
 */
package olioht;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Konsta
 */
public class FXMLErrorMessageController implements Initializable {
    @FXML
    private Button errorOkButton;
    @FXML
    private Label errorMessageLabel;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        System.out.println("Virhekoodi: " + FXMLCreatePackageController.errorCode);
        switch(FXMLCreatePackageController.errorCode) {
            case 1:
                errorMessageLabel.setText("Vaadittuja kenttiä ei ole täytetty \ntai ne on täytetty virheellisesti!");
                break;
            case 2:
                errorMessageLabel.setText("Paketin paino tai tilavuus tai kuljetusmatka\n on liian suuri!\nTarkasta luokkakohtaiset rajat.");
                break;
            case 3:
                errorMessageLabel.setText("Nimi on jo olemassa. Muuta nimi.");
                break;
            default:
                errorMessageLabel.setText("Virhe! Kokeile uudestaan.");
        }
        
    }    

    @FXML
    private void closeErrorWindow(ActionEvent event) {
        Stage stage = (Stage) errorOkButton.getScene().getWindow();
        stage.close();
    }
    
}
