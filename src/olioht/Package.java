/*
Abstract class for package: FirstClass, SecondClass and ThirdClass extend this
 */

package olioht;

import java.util.Date;

/**
 *
 * Konsta Ala-Ilomäki, 0418514
 */

public abstract class Package {
    
    protected Item contents;
    protected SmartPost departurePost;
    protected SmartPost arrivalPost;
    protected double distance;
    protected String state = "Odottaa lähetystä";
    protected Date timeOfCreation;
    protected String packageID;
    protected static int num = 1; //determines package ID
    protected int packageClass;
    
    protected Package(int clss, Item cntns, SmartPost dprt, SmartPost arr, double dist) {
        
        
        this.contents = cntns;
        this.departurePost = dprt;
        this.arrivalPost = arr;
        this.distance = dist;
        this.timeOfCreation = new Date();
        this.packageID = String.format("%04d", num);
        num++;
        
        //Add package to storage
        switch (clss) {
            case 1:
                this.packageClass = 1;
                Storage.firstClassPackages.add(this);
                break;
            case 2:
                this.packageClass = 2;
                Storage.secondClassPackages.add(this);
                break;
            case 3:
                this.packageClass = 3;
                Storage.thirdClassPackages.add(this);
                break;
         
        }
        Storage.addPackage();
    }

    public int getPackageClass() {
        return packageClass;
    }
    
    public Date getTimeOfCreation() {
        return timeOfCreation;
    }

    public String getPackageID() {
        return packageID;
    }
    
    /**
     *
     * @param i 0 = "Odottaa lähetystä", 1 = "Lähetetty"
     */
    public void changeState(int i) {
        switch(i) {
            case 0:
                this.state = "Odottaa lähetystä";
                break;
            case 1:
                this.state = "Lähetetty";
                break;
            default:
                this.state = "Odottaa lähetystä";
        }
    }
    
}
