/*
Storage class that keeps track of packages created
 */

package olioht;

import java.util.ArrayList;

/**
 *
 * Konsta Ala-Ilomäki, 0418514
 */
public class Storage {
    
    protected static ArrayList<Package> firstClassPackages = new ArrayList<>();
    protected static ArrayList<Package> secondClassPackages = new ArrayList<>();
    protected static ArrayList<Package> thirdClassPackages = new ArrayList<>();
    protected static ArrayList<Package> sentPackages = new ArrayList<>();
    protected static int receivedPackageAmount = 0; //The amount of packages created
    protected static int sentPackageAmount = 0; //The amount of packages sent
    
    protected Storage() { 
    }
    /**
     * Find out how many packages are in the storage
     * @return number of packages in storage
     */
    public static int whatsInStorage() {
        if (receivedPackageAmount-sentPackageAmount > 0)
            return receivedPackageAmount-sentPackageAmount;
        else
            return 0;
    }
    
    /**
     * Get the departure city of a package
     * @param s ID of a package
     * @return departure city and address
     */
    public static String getDeparture(String s) {
        for (Package p : firstClassPackages) {
            if (p.getPackageID().equals(s))
                return p.departurePost.getAddress() + " " + p.departurePost.getCity();
        }
        for (Package p : secondClassPackages) {
            if (p.getPackageID().equals(s))
                return p.departurePost.getAddress() + " " + p.departurePost.getCity();
        }
        for (Package p : thirdClassPackages) {
            if (p.getPackageID().equals(s))
                return p.departurePost.getAddress() + " " + p.departurePost.getCity();
        }
        return null;
    }
    
    /**
     * Get the arrival city of a package
     * @param s ID of a package
     * @return arrival city and address
     */
    public static String getArrival(String s) {
        for (Package p : firstClassPackages) {
            if (p.getPackageID().equals(s))
                return p.arrivalPost.getAddress() + " " + p.arrivalPost.getCity();
        }
        for (Package p : secondClassPackages) {
            if (p.getPackageID().equals(s))
                return p.arrivalPost.getAddress() + " " + p.arrivalPost.getCity();
        }
        for (Package p : thirdClassPackages) {
            if (p.getPackageID().equals(s))
                return p.arrivalPost.getAddress() + " " + p.arrivalPost.getCity();
        }
        return null;
    }
    
    /**
     * 
     * @param s ID of a package
     * @return Distance between departure post and arrival post
     */
    public static double getDistance(String s) {
        for (Package p : firstClassPackages) {
            if (p.getPackageID().equals(s))
                return p.distance;
        }
        for (Package p : secondClassPackages) {
            if (p.getPackageID().equals(s))
                return p.distance;
        }
        for (Package p : thirdClassPackages) {
            if (p.getPackageID().equals(s))
                return p.distance;
        }
        return 0;
    }
    
    /**
     * 
     * @param s ID of a package
     * @return Class of a package
     */
    public static int getClss(String s) {
        for (Package p : firstClassPackages) {
            if (p.getPackageID().equals(s))
                return 1;
        }
        for (Package p : secondClassPackages) {
            if (p.getPackageID().equals(s))
                return 2;
        }
        for (Package p : thirdClassPackages) {
            if (p.getPackageID().equals(s))
                return 3;
        }
        return 0;
    }
   
    /* Removes certain package from storage */
    public static void removePackage(Package p) {
        sentPackages.add(p);
        sentPackageAmount++;
    }
    
    /* Add package count*/
    public static void addPackage() {
        receivedPackageAmount++;
    }
    
    /**
     * 
     * @param s contents of a wanted package
     * @return Package with ID s
     */
    public static Package getPackage(String s) {
        for (Package p : firstClassPackages) {
            if (p.contents.getName().equals(s))
                return p;
        }
        for (Package p : secondClassPackages) {
            if (p.contents.getName().equals(s))
                return p;
        }
        for (Package p : thirdClassPackages) {
            if (p.contents.getName().equals(s))
                return p;
        }
        return null;
    }
}
