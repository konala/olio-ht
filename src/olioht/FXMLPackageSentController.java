/*
 Controller for package sent -window

 */
package olioht;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Konsta
 */
public class FXMLPackageSentController implements Initializable {
    @FXML
    private Button okButton;
    @FXML
    private Label infoLabel;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        switch(FXMLDocumentController.errorCode) {
            case 0:
                infoLabel.setText("Paketti pysyi ehjänä kuljetuksen ajan!");
                break;
            case 1:
                infoLabel.setText("Paketti hajosi kuljetuksessa!\nKokeile jotain toista kuljetusmuotoa!");
                break;
            case 2:
                infoLabel.setText("Paketti on jo lähetetty!\nValitse tai luo uusi paketti.");
                break;
            case 3:
                infoLabel.setText("Yhtään pakettia ei ole luotu!\nLuo ensin paketti.");
                break;
            case 4:
                infoLabel.setText("Valintaa ei ole tehty!");
                break;
            default:
                infoLabel.setText("Virhe");
        }
            
    }    

    @FXML
    private void closeWindow(ActionEvent event) {
        Stage stage = (Stage)okButton.getScene().getWindow();
        stage.close();
    }
    
}
