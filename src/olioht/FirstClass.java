/*
First class package
 */

package olioht;

/**
 *
 * Konsta Ala-Ilomäki, 0418514
 */
public class FirstClass extends Package{
    public FirstClass(int clss, Item cntns, SmartPost dprt, SmartPost arr, double dist) {
        super(clss, cntns, dprt, arr, dist);
    }
}
