/* Controller for creating new package with "Luo paketti.."-button */
package olioht;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Konsta
 */
public class FXMLCreatePackageController implements Initializable {
    //JavaFX components
    @FXML
    private RadioButton createNewItem;
    @FXML
    private ToggleGroup itemOptions;
    @FXML
    private RadioButton chooseFromExistingItemsButton;
    @FXML
    private ComboBox<String> itemCombobox;
    @FXML
    private TextField itemNameField;
    @FXML
    private TextField itemSizeField;
    @FXML
    private TextField itemMassField;
    @FXML
    private CheckBox fragileButton;
    @FXML
    private ToggleGroup classOptions;
    @FXML
    private Label classInfoLabel;
    @FXML
    private Button createPackageButton;
    @FXML
    private Button cancelPackageCreationButton;
    @FXML
    private ComboBox<String> placeOfDepartureCombobox;
    @FXML
    private ComboBox<String> placeOfArrivalCombobox;
    @FXML
    private Label distanceLabel;
    @FXML
    private Button calcDistanceButton;
    @FXML
    private ComboBox<String> departureKioskCombobox;
    @FXML
    private ComboBox<String> arrivalKioskCombobox;
   
    //Other variables
    public static Item item = null; 
    public static String departurePost = null;
    public static String arrivalPost = null;
    public static int packageClass = 0;
    public static int errorCode = 1;//errorCode determines whether or not a package can be created: 0 = yes, 1 = no, 2 = weight or volume or distance incorrect, 3 = item name already exists
    public static double routeLength = 0;
    Log log = Log.getInstance(); 
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        fillComboboxes();
    }    
    
    /* Sets the correct visibility for components needed to create new package from scratch */
    @FXML
    private void createNewItem(ActionEvent event) {
        itemCombobox.setVisible(false);
        itemNameField.setVisible(true);
        itemMassField.setVisible(true);
        itemSizeField.setVisible(true);
        fragileButton.setVisible(true);
    }
    
    /* Sets the correct visibility for componentst needed to create new package from existing items */
    @FXML
    private void chooseFromExistingItems(ActionEvent event) {
        itemNameField.setVisible(false);
        itemSizeField.setVisible(false);
        itemMassField.setVisible(false);
        itemCombobox.setVisible(true);
        fragileButton.setVisible(false);
    }

    
    /* Shows class info when one of the classes is selected */
    @FXML
    private void showClassInfo(ActionEvent event) {
        Toggle t = classOptions.getSelectedToggle();
        String s = t.toString();
        
        if (s.contains("1. luokka")) {
            classInfoLabel.setText("Tietoa valitsemastasi luokasta\nTilavuus: <1 m3\nPaino: <50 kg\nKuljetusmatka: <150 km\nHuom. Särkyvät esineet rikkoutuvat.\nNopein kuljetus.");
        } else if (s.contains("2. luokka")) {
            classInfoLabel.setText("Tietoa valitsemastasi luokasta\nTilavuus: <0,5 m3\nPaino: <10 kg\nKuljetusmatka: Kaikki käy\nHuom. 2. lk on pienille paketeille.\nPaketit eivät rikkoudu matkalla.\nNopeudeltaan keskiluokkaa.");
        } else if (s.contains("3. luokka")) {
            classInfoLabel.setText("Tietoa valitsemastasi luokasta\nTilavuus: Kaikki käy\nPaino: Kaikki käy\nKuljetusmatka: Kaikki käy\nHuom. 3. lk on suurille ja painaville\npaketeille.\nSärkyvät paketit rikkoutuvat matkalla.\nHitain kuljetusmuoto.");
        }
        
    }

    /* pressing "Luo paketti"-button */
    @FXML
    private void createPackage(ActionEvent event) {
       
        double distance = 0;
        Package p = null;
        
        //1. creating package from scratch, First check that required fields are filled
        if(classOptions.getSelectedToggle() != null && departureKioskCombobox.getValue() != null && arrivalKioskCombobox.getValue() != null) {
            //Checking more fields are filled
            if (createNewItem.isSelected() && !(itemNameField.getText().isEmpty()) && !(itemSizeField.getText().isEmpty()) && !(itemMassField.getText().isEmpty())) {
                System.out.println("Luodaan paketti");
                errorCode = 0;
                //Setting the attributes of a package
                boolean fragile = fragileButton.isSelected();
                String departure = departureKioskCombobox.getValue();
                String arrival = arrivalKioskCombobox.getValue();
                String[] s = itemSizeField.getText().split("x");
                
                
                    
                double volume = 0;
                double mass = 0;
                ArrayList<String> path = new ArrayList<>();
                path.add(SmartPost.getSmartPost(departure).getLatitude());
                path.add(SmartPost.getSmartPost(departure).getLongitude());
                path.add(SmartPost.getSmartPost(arrival).getLatitude());
                path.add(SmartPost.getSmartPost(arrival).getLongitude());
                DataBuilder db = new DataBuilder(path);
                distance = Double.parseDouble(db.getDistance())/1000;
                //Error checking for mass and volume fields
                try {
                    mass = Double.parseDouble(itemMassField.getText());
                    double height = Double.parseDouble(s[0]);
                    double width = Double.parseDouble(s[1]);
                    double depth = Double.parseDouble(s[2]);
                    volume = height*width*depth;
                } catch (ArrayIndexOutOfBoundsException | NumberFormatException e) {
                    System.out.println("Virhe");
                    errorCode = 1;    
                }
                
                //Error check for duplicate item names
                String cs;
                
                for (Item it : Item.getItems()) {
                    if (item != null) {
                        cs = item.getName();
                        if (cs.equals(itemNameField.getText()))
                            errorCode = 3; 
                    }
                }
                
                //If all went according to plans(=errorCode 0), a new item can be created
                if (errorCode == 0) {    
                    Item contents = new Item(mass, volume, itemNameField.getText(), fragile);
                    item = contents;
                    departurePost = departure;
                    arrivalPost = arrival;
                    Toggle t = classOptions.getSelectedToggle();
                    String ts = t.toString();
                    
                    //Choose the right class and create package with the contents created above, change errorCode if the item exceeds the limits set for certain class (volume, mass distance)
                    if (ts.contains("1. luokka")) {
                        if (mass < 50 && mass > 0 && volume < 1000000 && volume > 0 && distance <= 150) {
                            FirstClass fc = new FirstClass(1, contents, SmartPost.getSmartPost(departure), SmartPost.getSmartPost(arrival), distance);
                            p = fc;
                            
                            packageClass = 1;
                        } else {
                            errorCode = 2;
                            
                        }
                    } else if (ts.contains("2. luokka")) {
                        if (mass < 10 && mass > 0 && volume < 500000 && volume > 0) {
                            SecondClass sc = new SecondClass(2, contents, SmartPost.getSmartPost(departure), SmartPost.getSmartPost(arrival), distance);
                            p = sc;
                            packageClass = 2;
                        } else {
                            errorCode = 2;
                            
                        }
                    } else if (ts.contains("3. luokka")) {
                        if (mass > 0 && volume >0) {
                            ThirdClass tc = new ThirdClass(3, contents, SmartPost.getSmartPost(departure), SmartPost.getSmartPost(arrival), distance);
                            p = tc;
                            packageClass = 3;
                        } else {
                            errorCode = 2;
                            
                        }
                    }
                    
                    
                }
            //2. creating package from existing items   
            } else if (chooseFromExistingItemsButton.isSelected() && itemCombobox.getValue() != null) {
                System.out.println("Luodaan paketti");
                errorCode = 0;
                
                Item contents = Item.getItem(itemCombobox.getValue());
                String departure = departureKioskCombobox.getValue();
                String arrival = arrivalKioskCombobox.getValue();
                item = contents;
                departurePost = departure;
                arrivalPost = arrival;
                ArrayList<String> path = new ArrayList<>();
                path.add(SmartPost.getSmartPost(departure).getLatitude());
                path.add(SmartPost.getSmartPost(departure).getLongitude());
                path.add(SmartPost.getSmartPost(arrival).getLatitude());
                path.add(SmartPost.getSmartPost(arrival).getLongitude());
                DataBuilder db = new DataBuilder(path);
                distance = Double.parseDouble(db.getDistance())/1000;
                
                if (contents != null) {
                    Toggle t = classOptions.getSelectedToggle();
                    String ts = t.toString();
                    
                   if (ts.contains("1. luokka")) {
                        if (contents.getWeight() < 50 && contents.getWeight() > 0 && contents.getVolume() < 1000000 && contents.getVolume() > 0 && distance <= 150) {
                            FirstClass fc = new FirstClass(1, contents, SmartPost.getSmartPost(departure), SmartPost.getSmartPost(arrival), distance);
                            p = fc;
                            packageClass = 1;
                        } else {
                            errorCode = 2;
                            
                        }
                    } else if (ts.contains("2. luokka")) {
                        if (contents.getWeight() < 10 && contents.getWeight() > 0 && contents.getVolume() < 500000 && contents.getVolume() > 0) {
                            SecondClass sc = new SecondClass(2, contents, SmartPost.getSmartPost(departure), SmartPost.getSmartPost(arrival), distance);
                            p = sc;
                            packageClass = 2;
                        } else {
                            errorCode = 2;
                            
                        }
                    } else if (ts.contains("3. luokka")) {
                        if (contents.getWeight() > 0 && contents.getVolume() >0) {
                            ThirdClass tc = new ThirdClass(3, contents, SmartPost.getSmartPost(departure), SmartPost.getSmartPost(arrival), distance);
                            p = tc;
                            packageClass = 3;
                        } else {
                            errorCode = 2;
                            
                        }
                    }
                    
                }
                
                
                
            }
        }
        //Finally pop up error message if something went wrong
        if (errorCode != 0) {
            raiseError();
        //Or if a package was created write to log and return to main window
        } else {
            routeLength = distance;
            log.writeToLog(p, 0);
            creationSuccesful();
            
            
            
            
        }
        errorCode = 1;
        
    }
    /* Close package creation window */
    @FXML
    private void cancelPackageCreation(ActionEvent event) {
        Stage stage = (Stage) cancelPackageCreationButton.getScene().getWindow();
        stage.close();
    }
    
    /* Method for creating an error message */
    private void raiseError() {
        try {
                Stage errorMessage = new Stage();
                Parent parent = FXMLLoader.load(getClass().getResource("FXMLErrorMessage.fxml"));
                Scene scene = new Scene(parent);
                Image warning = new Image(getClass().getResource("warning_icon.png").toExternalForm());
                errorMessage.getIcons().add(warning);
                scene.getStylesheets().add(getClass().getResource("CSSWarning.css").toExternalForm());
                errorMessage.setTitle("Virhe");
                errorMessage.setScene(scene);
                errorMessage.show();
            } catch (IOException ex) {
                Logger.getLogger(FXMLCreatePackageController.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    
    /* Method for "creation succeful" window */
    private void creationSuccesful() {
        try {
            Stage creationMessage = new Stage();
            Parent parent = FXMLLoader.load(getClass().getResource("FXMLCreationSuccesful.fxml"));
            Scene scene = new Scene(parent);
            scene.getStylesheets().add(getClass().getResource("CSSSuccesful.css").toExternalForm());
            Image check = new Image(getClass().getResource("check.png").toExternalForm());
            creationMessage.getIcons().add(check);
            creationMessage.setTitle("Paketin luominen onnistui!");
            creationMessage.setScene(scene);
            creationMessage.show();
            Stage stage = (Stage)createPackageButton.getScene().getWindow();
            stage.close();
        } catch (IOException ex) {
                Logger.getLogger(FXMLCreatePackageController.class.getName()).log(Level.SEVERE, null, ex);
            }
       
        
    }
    
    
    private void fillComboboxes() {
        itemCombobox.getItems().clear();
        placeOfDepartureCombobox.getItems().clear();
        placeOfArrivalCombobox.getItems().clear();
        departureKioskCombobox.getItems().clear();
        arrivalKioskCombobox.getItems().clear();
        
       //Premade items are added
        if (Item.getItems().isEmpty()) {    
            Item i1 = new Item(0.5, 10, "Puukko", false);
            Item i2 = new Item(1.2, 100, "Nukke", true);
            Item i3 = new Item(5, 200, "AWP Tarkkuuskivääri", false);
            Item i4 = new Item(2, 150, "Läppäri", true);
            Item i5 = new Item(0.2, 50, "Kenkä", false);
            Item i6 = new Item(90, 500, "Elintasopakolainen", true);
            Item i7 = new Item(150, 1000, "Teline", false);
        }
        for (Item i : Item.getItems()) {
            itemCombobox.getItems().add(i.getName());
        }
        
        for (SmartPost s : SmartPost.getSmartPosts()) {
            if (!(placeOfDepartureCombobox.getItems().contains(s.getCity())))
                placeOfDepartureCombobox.getItems().add(s.getCity());
        }
        for (SmartPost s : SmartPost.getSmartPosts()) {
            if (!(placeOfArrivalCombobox.getItems().contains(s.getCity())))
                placeOfArrivalCombobox.getItems().add(s.getCity());
        }
        placeOfDepartureCombobox.getItems().sort(null);
        placeOfArrivalCombobox.getItems().sort(null);
    }

    /* method that uses coordinates and DataBuilder to determine distance between two smartposts */
    @FXML
    private void updateDistance(ActionEvent event) {
        if (departureKioskCombobox.getValue() != null && arrivalKioskCombobox.getValue() != null) {
            String dpLng = SmartPost.getSmartPost(departureKioskCombobox.getValue()).getLatitude();
            String dpLat = SmartPost.getSmartPost(departureKioskCombobox.getValue()).getLongitude();
            String arLng = SmartPost.getSmartPost(arrivalKioskCombobox.getValue()).getLatitude();
            String arLat = SmartPost.getSmartPost(arrivalKioskCombobox.getValue()).getLongitude();
            ArrayList<String> coords = new ArrayList<>();
            coords.add(dpLng);
            coords.add(dpLat);
            coords.add(arLng);
            coords.add(arLat);
            
            DataBuilder db = new DataBuilder(coords);
       
            distanceLabel.setText("Välimatka: " + Double.parseDouble(db.getDistance())/1000 + " km");
        }
            
    }
    
    
    @FXML
    private void updateDepKioskCombobox(ActionEvent event) {
        departureKioskCombobox.getItems().clear();
        if (placeOfDepartureCombobox.getValue() != null) {
            for (SmartPost sp : SmartPost.getSmartPosts()) {
                if (sp.getCity().equals(placeOfDepartureCombobox.getValue())) {
                    departureKioskCombobox.getItems().add(sp.getPostoffice() + ", " + sp.getAddress());
                }
            }
        }
       
    }

    @FXML
    private void updateArrKioskCombobox(ActionEvent event) {
         arrivalKioskCombobox.getItems().clear();
         
         if (placeOfArrivalCombobox.getValue() != null) {
            for (SmartPost sp : SmartPost.getSmartPosts()) {
                if (sp.getCity().equals(placeOfArrivalCombobox.getValue())) {
                    arrivalKioskCombobox.getItems().add(sp.getPostoffice() + ", " + sp.getAddress());
                   
                }
            }
           
        }
    }
    
    
}
