/*
Class for smartposts
 */

package olioht;

import java.util.ArrayList;

/**
 *
 * Konsta Ala-Ilomäki, 0418514
 */

public class SmartPost {
    private static ArrayList<SmartPost> smartPosts = new ArrayList<>();
    private String code;
    private String city;
    private String address;
    private String postoffice;
    private String availability;
    private String latitude;
    private String longitude;
    
    
    public SmartPost(String cd, String ct, String add, String po, String ab, String lat, String lng) {
        this.code = cd;
        this.city = ct;
        this.address = add;
        this.postoffice = po;
        this.availability = ab;
        this.latitude = lat;
        this.longitude = lng;
        smartPosts.add(this);
    }
    
    
    public SmartPost() {
        System.out.println("Luodaan smartpost ilman tietoja.");
    }

    public static ArrayList<SmartPost> getSmartPosts() {
        return smartPosts;
    }

    public String getCode() {
        return code;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getPostoffice() {
        return postoffice;
    }
    
    public String getAvailability() {
        return availability;
    }
    
    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    /**
     * Searches for a certain smartpost in smartPosts.
     * @param s = Information about the wanted smartpost. Must include at least address.
     * @return  SmartPost-object or null if no matching SmartPost was found
     */
    public static SmartPost getSmartPost(String s) {
         for (SmartPost sp : smartPosts) {
             if (s.toLowerCase().contains(sp.address.toLowerCase()) || sp.address.toLowerCase().contains(s.toLowerCase()))
                 return sp;
         }
         return null;
     }
    
    
    
    
    
}
